import 'Shape.dart';

class Rectangle extends Shape {
  double w;
  double h;

  Rectangle(this.w, this.h) : super('Rectangle');

  @override
  double calArea() {
    return w * h;
  }

  @override
  String toString() {
    return "Rectangle{ w = $w  h = $h }";
  }
}
