abstract class Shape {
  String name;

  Shape(this.name);

  String toString() {
    return "Shape{name = $name}";
  }

  double calArea();
}
