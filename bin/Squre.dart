import 'Shape.dart';

class Sqaure extends Shape {
  double side;

  Sqaure(this.side) : super('Sqaure');

  @override
  double calArea() {
    return side * side;
  }

  @override
  String toString() {
    return "Sqaure{ side = $side }";
  }
}
