import 'Shape.dart';

class Circle extends Shape {
  double r;
  double pi = 22 / 7;

  Circle(this.r) : super("Circle");

  @override
  double calArea() {
    double sum = pi * r * r;
    return sum;
  }

  @override
  String toString() {
    return "Circle{ r = $r }";
  }
}
