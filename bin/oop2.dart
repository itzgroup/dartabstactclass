import 'Circle.dart';
import 'Rectangle.dart';
import 'Shape.dart';
import 'Squre.dart';

void main() {
  var c1 = new Circle(3);
  print(c1);
  print(c1.calArea().toStringAsFixed(2));
  var r1 = new Rectangle(2, 4);
  print(r1);
  print(r1.calArea());
  var s1 = new Sqaure(4);
  print(s1);
  print(s1.calArea());
}
